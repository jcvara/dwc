"use strict";

var input;

$(document).ready(function() {

  //Exercise 1
  $("#btn-count-ex1").click(function() {
    window.alert("Hay " + $(".div-ex1").length + " capas.");
  });

  $("#btn-color-ex1").click(function() {
    $(".div-ex1").css("color", "green");
  });

  //Exercise 2
  $("#btn-show-ex2").click(function() {
    $("#img-ex2").show();
  });

  $("#btn-hide-ex2").click(function() {
    $("#img-ex2").hide();
  });

  //Exercise 3
  $("input[type=radio][name=color-ex3]").change(function() {
    $("#text-color").css("color", this.value);
  });

  //Exercise 4
  $("#chk-ex4").change(function() {

    if($(this).prop("checked")) {

      input = document.createElement("input");
      input.type = "text";
      input.className = "textbox";
      input.id = "result-ex4";
      input.value = parseInt($("#in-sum-1-ex4").val(), 10) + parseInt($("#in-sum-2-ex4").val(), 10);

      $("#frm-ex4").append(input);

    } else {

      $("#result-ex4").remove();

    }

  });

  //Exercise 5
  $("#txt-ex5").hover(function() {
    $(this).toggleClass("big-text");
  });

  //Exercise 6
  $("#btn-fx-ex6").click(function() {

    $("#img-ex6").fadeOut(2000, function() {

      $(this).slideDown(3000, function() {

        $(this).slideUp(1000);

      });

    });

  });

});
