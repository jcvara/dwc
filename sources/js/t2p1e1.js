//--------[jshint]--------
/* jshint -W083 */
/* global ActiveXObject */
/* exported ajaxIO */
//--------[jshint]--------
"use strict";

var buttons = document.getElementsByClassName("submit");

function ajaxIO(method, file)
{

  var xmlhttp;

  if (window.XMLHttpRequest) {

    xmlhttp = new XMLHttpRequest();

  } else {

    xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");

  }

  xmlhttp.onreadystatechange = function()
  {

    if (xmlhttp.readyState === 4 && xmlhttp.status === 200) {

      var newResult = document.createElement("p");
      newResult.setAttribute("id", "text");

      var oldResult = document.getElementById("text");

      newResult.appendChild(document.createTextNode(xmlhttp.responseText));

      document.getElementById("text-container").replaceChild(newResult, oldResult);

    }

  };

  xmlhttp.open(method, file);
  xmlhttp.send(null);

}


for (var i = buttons.length - 1; i >= 0; i--)
{

  (function(i) {

    var id = buttons[i].id.split("-");

    if (id[0] === 'text')
    {
      buttons[i].addEventListener("click", function() {ajaxIO('GET', 'text' + id[1] + '.txt');}, false);
    }

  })(i);

}
