"use strict";

$.ajaxSetup({cache: false});

var header,
    title,
    description,
    url;

$(document).ready(function() {

  $("#show-text").click(function() {

    $("#page-container").html("");

    $.get("page1.xml", function (xml) {

      $(xml).find("blog").each(function () {

        header = document.createElement("h3");
        header.textContent = $(this).attr('name');

        title = document.createElement("p");
        title.appendChild(header);

        description = document.createElement("p");
        description.textContent = $(this).attr('description');

        url = document.createElement("a");
        url.text = "Enlace";
        url.href = $(this).attr('url');

        $("#page-container").append(title, description, url);

      });

    });

  });

});
