//--------[jshint]--------
/* exported popup */
//--------[jshint]--------
"use strict";

function toggle(divId)
{

	var popup = document.getElementById(divId);

	if(popup.style.display === 'none' || popup.style.display === '')
	{
		popup.style.display = 'block';
	}
	else
	{
		popup.style.display = 'none';
	}

}

function blanketSize(popUpDivVar)
{

	var viewportHeight,
			blanketHeight;

	if(typeof window.innerWidth !== 'undefined')
	{
		viewportHeight = window.innerHeight;
	}
	else
	{
		viewportHeight = document.documentElement.clientHeight;
	}

	if((viewportHeight > document.body.parentNode.scrollHeight) && (viewportHeight > document.body.parentNode.clientHeight))
	{
		blanketHeight = viewportHeight;
	}
	else
	{

		if(document.body.parentNode.clientHeight > document.body.parentNode.scrollHeight)
		{
			blanketHeight = document.body.parentNode.clientHeight;
		}
		else
		{
			blanketHeight = document.body.parentNode.scrollHeight;
		}

	}

	var blanket = document.getElementById('blanket');
	blanket.style.height = blanketHeight + 'px';

}

function popup(windowname)
{

	blanketSize(windowname);
	toggle('blanket');
	toggle(windowname);

}
