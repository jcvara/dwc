"use strict";

$.ajaxSetup({cache: false});

$(document).ready(function() {

  $("#show-text").click(function() {

    var ajax = $.ajax({
        type: "GET",
        url: "page1.php",
        dataType: "html",
        beforeSend: function() {$("html").css("cursor", "wait");}
    });

    ajax.done(function(data) {
        $("#page-container").html(data);
        $("html").css("cursor", "auto");
    });

    ajax.fail(function() {
        window.alert("La operacion ha fallado...");
        $("html").css("cursor", "auto");
    });

  });

});
