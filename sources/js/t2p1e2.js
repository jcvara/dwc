//--------[jshint]--------
/* jshint -W083 */
/* global ActiveXObject */
/* exported ajaxIO */
//--------[jshint]--------
"use strict";

var buttons = document.getElementsByClassName("submit");

function ajaxIO(method, file)
{

  var xmlhttp;

  if (window.XMLHttpRequest) {

    xmlhttp = new XMLHttpRequest();

  } else {

    xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");

  }

  xmlhttp.onreadystatechange = function()
  {

    if (xmlhttp.readyState === 4 && xmlhttp.status === 200) {

      document.getElementById("page-container").innerHTML = xmlhttp.responseText;

    }

  };

  xmlhttp.open(method, file);
  xmlhttp.send(null);

}


for (var i = buttons.length - 1; i >= 0; i--)
{

  (function(i) {

    var id = buttons[i].id.split("-");

    if (id[0] === 'page')
    {
      buttons[i].addEventListener("click", function() {ajaxIO('GET', 'page' + id[1] + '.html');}, false);
    }

  })(i);

}
