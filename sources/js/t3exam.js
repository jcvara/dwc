//--------[jshint]--------
/* jshint -W083 */
/* global ActiveXObject */
/* exported ajaxIO */
//--------[jshint]--------
"use strict";

//-----------------------------------------------------------------------------
function ajaxIO(method, file)
{

  var xmlhttp;

  if (window.XMLHttpRequest) {

    xmlhttp = new XMLHttpRequest();

  } else {

    xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");

  }

  xmlhttp.onreadystatechange = function()
  {

    if (xmlhttp.readyState === 4 && xmlhttp.status === 200) {

      var newResult = document.createElement("p");
      newResult.setAttribute("id", "text");
      newResult.innerHTML = xmlhttp.responseText;

      var oldResult = document.getElementById("text");


      document.getElementById("text-container").replaceChild(newResult, oldResult);

    }

  };

  xmlhttp.open(method, file);
  xmlhttp.send(null);

}
//-----------------------------------------------------------------------------

$(document).ready(function() {

  ajaxIO("GET", "../assets/files/terms.html");

  $("#textChange").click(function() {
    $("#text").textChange();
  });

  var h = document.createElement("h1");
  h.innerHTML = "OFERTAS DEL MES";

  $("#div2").prepend(h);

  $("#computer").click(function() {
    $(this).fadeOut().delay(1500).fadeIn();
  });

  $("input[type=radio][name=offers]").change(function() {

    $.get("../assets/files/offer" + this.value + ".html", function(data) {
      $("#offer-text").html(data);
    });

    });

});
