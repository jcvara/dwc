"use strict";

$.ajaxSetup({cache: false});

//---------------------------------------
var functions = [];

functions.hide = function() {
  $("img").hide("fast");
};

functions.show = function() {
  $("img").show("fast");
};

functions.fadeOut = function() {
  $("img").fadeOut("fast");
};

functions.fadeIn = function() {
  $("img").fadeIn("fast");
};

functions.fadeTo = function() {
  $("img").fadeTo(
    "slow",
    0,
    function() {
      $("img").fadeTo("fast", 1);
    }
  );
};

functions.slideDown = function() {
  $("img").slideDown("fast");
};

functions.slideUp = function() {
  $("img").slideUp("fast");
};

functions.animate = function() {
  $("img").animate(
    {blurRadius: 20},
    {
      duration: 1000,
      easing: 'swing',
      step: function() {
        $("img").css(
          {
            "-webkit-filter": "blur("+this.blurRadius+"px)",
            "filter": "blur("+this.blurRadius+"px)"
          }
        );
      }
    }
  );
};
//---------------------------------------

$(document).ready(function() {

  $("button").each(function() {

    $(this).click(functions[this.id]);

  });

});
