"use strict";

$.ajaxSetup({cache: false});

$(document).ready(function() {

  $("#show-text").click(function() {

    var ajax = $.ajax({
        type: "GET",
        url: "page1.html",
        dataType: "html",
        beforeSend: function () {window.alert("Cargando...");}
    });

    ajax.done(function(data) {
      $("#page-container").html(data);
    });

    ajax.fail(function() {
      window.alert("La operacion ha fallado...");
    });

  });

});
