"use strict";

$.ajaxSetup({cache: false});

$(document).ready(function() {

  $("#show-text").click(function() {

    $.ajax({
      type: "GET",
      url: "page1.html",
      dataType: "html",
      success: function(data) {
        $("#page-container").html(data);
      }
    });

  });

});
