jQuery.fn.hoverEffect = function()
{

  this.each(function()
  {

    $(this).data('original-size', $(this).css('fontSize'));

    $(this).mouseenter(function() {

      $(this).animate({
        fontSize: "40px"
      }, 1000);

    });

    $(this).mouseleave(function() {

      var originalSize = $(this).data('original-size');

      if ($(this).css('fontSize') != originalSize) {

        $(this).animate({
          fontSize: originalSize
          }, 500);

      }

    });

  });

  return this;

};
