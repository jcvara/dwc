var paths = {
    src: 'sources/',
    dest: 'public_html/',
    assets: 'public_html/assets/'
};

module.exports = {
  paths: {
    src: paths.src,
    dest: paths.dest,
    assets: paths.assets
  },
  scripts: {
    src: paths.src + 'js/',
    dest: paths.assets + 'js/'
  },
  styles: {
    src: paths.src + 'scss/',
    dest: paths.assets + 'css/',
    settings: {
      errLogToConsole: false,
      includePaths: require('node-refills').includePaths,
      style: 'expanded'
    }
  },
  html: {
    src: paths.src + 'html/**/',
    dest: paths.dest,
    settings: {
      charEncoding: 'utf8',
      doctype: 'html5',
      dropEmptyElements: false,
      dropEmptyParas: false,
      hideComments: true,
      indent: false,
      mergeDivs: false,
      newBlocklevelTags: 'main'
    }
  }
};
