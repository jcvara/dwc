var gulp = require('gulp'),
    changed = require('gulp-changed'),
    notify = require('gulp-notify'),
    tidy = require('gulp-htmltidy'),
    html = require('../config').html,
    handleErrors = require('../util/handleErrors');

gulp.task('html', function() {
  return gulp.src(html.src + '*.html')
  .pipe(changed(html.dest))
  .pipe(tidy(html.settings))
  .on('error', handleErrors)
  .pipe(gulp.dest(html.dest))
  .pipe(notify({
    message: 'HTML task complete'
  }));
});
